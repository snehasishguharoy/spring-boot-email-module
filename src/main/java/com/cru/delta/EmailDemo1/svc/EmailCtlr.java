package com.cru.delta.EmailDemo1.svc;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cru.delta.EmailDemo1.domain.EmailMessage;

@RestController
public class EmailCtlr {
	@Value("${gmail.username}")
	private String userName;
	@Value("${gmail.password}")
	private String password;
	
	
	@RequestMapping(value="/send",method=RequestMethod.POST)
	public String sendMail(@RequestBody EmailMessage emailMessage) throws AddressException, MessagingException, IOException{
		sendEmail(emailMessage);
		return "Email Send Successfully";
		
		
	}
	
	private void sendEmail(EmailMessage emailMessage) throws AddressException, MessagingException, IOException{
		Properties pros=new Properties();
		pros.put("mail.smtp.auth", "true");
		pros.put("mail.smtp.starttls.enable","true");
		pros.put("mail.smtp.host", "smtp.gmail.com");
		pros.put("mail.smtp.port", "587");
		Session  session=Session.getInstance(pros,new javax.mail.Authenticator(){
			protected PasswordAuthentication getPasswordAuthentication(){
				return new PasswordAuthentication(userName, password);
			}
		});
		Message message=new MimeMessage(session);
		message.setFrom(new InternetAddress(userName,false));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("snehasishguharoy871@gmail.com"));
		message.setSubject(emailMessage.getSubject());
		message.setContent(emailMessage.getBody(), "text/html");
		message.setSentDate(new Date());
		MimeBodyPart bodyPart=new MimeBodyPart();
		bodyPart.setContent(emailMessage.getBody(), "text/html");
		Multipart multipart=new MimeMultipart();
		multipart.addBodyPart(bodyPart);
		MimeBodyPart mimeBodyPart=new MimeBodyPart();
		mimeBodyPart.attachFile("C:\\Users\\SNEHASISH GUHA ROY\\Desktop\\Photo.jpg");
		multipart.addBodyPart(mimeBodyPart);
		message.setContent(multipart);
		Transport.send(message);
			
			
		
		
	}

}
