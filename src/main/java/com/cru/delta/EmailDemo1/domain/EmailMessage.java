package com.cru.delta.EmailDemo1.domain;

public class EmailMessage {
	
	private String to_Address;
	private String subject;
	private String body;
	public String getTo_Address() {
		return to_Address;
	}
	public void setTo_Address(String to_Address) {
		this.to_Address = to_Address;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	

}
